#include "DemoLib/demolib.h"

#include <iostream>

DemoLib::DemoClass::DemoClass(QObject *parent)
    : QObject(parent),
    x_(-1)
{
}

DemoLib::DemoClass::~DemoClass() = default;

QString DemoLib::DemoClass::MyStaticString = "blub";

int DemoLib::DemoClass::z() const
{
    return x_/2;
}

int DemoLib::DemoClass::y() const
{
    return 2*x_;
}

int DemoLib::DemoClass::x() const
{
    return x_;
}

void DemoLib::DemoClass::setX(int x)
{
    x_ = x;
}

void DemoLib::DemoClass::customEvent(QEvent *event)
{
    QObject::customEvent(event);

    std::cerr << "custom Event" << std::endl;
}
