#pragma once

#include <QObject>

namespace DemoLib {

class Q_DECL_EXPORT DemoClass : public QObject
{
    Q_OBJECT

    public:
        DemoClass(QObject *parent = nullptr);
        ~DemoClass();

        int x() const;

        int y() const;
        int z() const;

        void setX(int x);

        void customEvent(QEvent *event) override;

        static QString MyStaticString;

    private:
        int x_;
};

}
